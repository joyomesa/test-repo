<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', "AuthController@home");
Route::get('/home', "AuthController@home");
Route::get('/signup',"AuthController@create");
Route::post('/post-signup','AuthController@store');
Route::get('/login','AuthController@login');
Route::post('/post-login','AuthController@postlogin');

Route::get("/logout","AuthController@logout");
Route::get('/getproducts',"ProductsController@index");