 <nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">Joy's Kitchen</a>
    </div>
    <ul class="nav navbar-nav">
      <li class="active"><a href="#">Home</a></li>
      <li><a href="#">About Us</a></li>
      <li><a href="#">Contact Us</a></li>
    </ul>
    <ul class="nav navbar-nav navbar-right">
        <li class="nav-item dropdown" id="accountdropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
         <span class="glyphicon glyphicon-user"></span>User Account
        </a>
        <div class="row  dropdown-menu user-account" aria-labelledby="navbarDropdown">
            <div class="col-md-6">
                @if(!Auth::check())
                 <a href="{{url('login')}}"> <span class="glyphicon glyphicon-off"> </span>Login</a>
                 @else
                  <a href="{{url('logout')}}"> <span class="glyphicon glyphicon-off"> </span>Logout</a>
                 @endif               
            </div>
            <div class="col-md-6">
                @if(!Auth::check())
                <a href="{{url('signup')}}"> <span class="glyphicon glyphicon-user"></span>Signup</a>
                @else
                <span>{{ucfirst(Auth()->user()->name)}}</span>
                @endif
            </div>
        </div>
      </li>
     
      <li>
        <a href="#">
            <span class="glyphicon glyphicon-shopping-cart"></span>
            <div class="cart" style="background-color: red; border-radius: 50%; color: white; width: 20px; height: 20px; display:flex; align-items: center; text-align: center;position: absolute;top: 0; right: 0;">
                <span id="cart-holder">0</span>
            </div>
        </a>
    </li>
    </ul>
  </div>
</nav> 