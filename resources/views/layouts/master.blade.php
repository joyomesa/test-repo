<!doctype html>
    <html xmlns="http://www.w3.org/1999/html">
        <head>
        <meta charset="UTF-8">
        <title>@yield('title')</title>
          <meta charset="utf-8">
          <meta name="csrf-token" content="{{ csrf_token() }}">

          <meta name="viewport" content="width=device-width, initial-scale=1">

         <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <link rel="stylesheet" href="{{url('css',['styles.css'])}}">
        
         <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
         <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="https://parsleyjs.org/dist/parsley.min.js"></script>
          

        </head>
    <body>
    @include('partials.header')
     @yield('content')

    
  
   

    </body>
    <script src=" https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.js"></script>
    <script src="{{url('js',['scripts.js'])}}"></script>
</html>
