
@extends('layouts.master')
@section('title')
	Signup - Joy's Kitchen
@endsection

@section('content')
<div class="row">
	<div class="col-sm-2"></div>
	<div class="col-sm-8">

		<h2 class="text-mute">Signup</h2>
		<div class="alert alert-success statusmsg" style="display: none;">
			<button type="button" class="close" data-dsimiss="alert">x</button>
			 <strong><span id="successmsg" class="text-success"></span></strong>
		</div>
		<div class="signupdiv well">
			<form action="" method="POST" id ="signupform">
				<div class="form-group">
					<label for="fullname"> Full Name</label>
					<input class="form-control" type="text" name="fullname" id="fullname" required="" data-parsley-trigger="keyup" data-parsley-required-message="Full name is required!" >
				</div>

				<div class="form-group">
					<label for="fullname"> Email</label>
					<input class="form-control" type="text" name="email" id="email" required="" data-parsley-type="email" data-parsley-trigger="keyup" data-parsley-required-message="Email is required!">
				</div>

				<div class="form-group">
					<label for="fullname"> Password</label>
					<input class="form-control" type="password" name="password" id="password" required data-parsley-trigger="keyup" data-parsley-required-message="Password is required!">
				</div>

				<div class="form-group">
					<label for="fullname"> Confirm Password</label>
					<input class="form-control" type="password" name="confirmpassword" id="confirmpassword" required="" data-parsley-trigger="keyup" data-parsley-required-message="Confirm your password" >
				</div>

				<div class="form-group">
					<input class='btn btn-primary' type="submit" value="submit" id="submit" />
				</div>
			</form>
		</div>
	</div>
	<div class="col-sm-2"></div>
</div>
@endsection