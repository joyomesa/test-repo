@extends('layouts.master')
@section('title')
Login - Joy's Kitchen
@endsection

@section('content')
<div class="container-login" style="background-color: yellow; ">
<div class="row" >
	<div class="col-sm-2"></div>
	<div class="col-sm-8">
		<div class="panel panel-default well userlogin">
		<div class="panel panel-header form-header" >Login</div>
		<div class="panel panel-body ">
		<div class="alert alert-success statusmsg" style="display: none;">
			<button type="button" class="close" data-dsimiss="alert">x</button>
			 <strong><span id="successmsg" class="text-success"></span></strong>
		</div>
		
			<form action="" method="POST" id ="loginform">
			
				<div class="form-label-group">
					<label for="fullname"> Email</label>
					<input class="form-control" type="text" name="email" id="email" required="" data-parsley-type="email" data-parsley-trigger="keyup" data-parsley-required-message="Email is required!">
				</div>

				<div class="form-label-group">
					<label for="fullname"> Password</label>
					<input class="form-control" type="password" name="password" id="password" required data-parsley-trigger="keyup" data-parsley-required-message="Password is required!">
				</div>

				<div class="form-label-group">
					<input class='btn btn-primary' type="submit" value="Login" id="submit" />
				</div>
			</form>
			<div class="row">
				<div class="col-sm-6">
					Not registered? <a href="{{url('signup')}}">Signup</a>
				</div>
					<div class="col-sm-6">
						Forgot Password? <a href="{{url('resetpassword')}}"> Reset</a>
					</div>
			</div>
		</div>
	</div>
	<div class="col-sm-2"></div>
</div>
</div>
@endsection