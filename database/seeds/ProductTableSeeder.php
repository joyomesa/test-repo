<?php

use Illuminate\Database\Seeder;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $product = new \App\Product([
        	'imagePath'=>'https://livforcake.com/wp-content/uploads/2017/06/vanilla-cake-thumb.jpg" alt="..." class="img-responsive',
        	'product_name'=>'Cakes',
        	'description'=>'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolores error eum inventore officia quis quos totam! Asperiores deleniti, distinctio illum incidunt nulla officiis quas suscipit vitae? Magni necessitatibus repellendus voluptate!',
        	'price'=>'1050',
            'status'=>'Active'

        ]);
        $product->save();
    
      
        $product = new \App\Product([
        	'imagePath'=>'https://thefirstyearblog.com/wp-content/uploads/2017/12/Chocolate-Velvet-Cake-7.jpg',
        	'product_name'=>'Cakes',
        	'description'=>'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolores error eum inventore officia quis quos totam! Asperiores deleniti, distinctio illum incidunt nulla officiis quas suscipit vitae? Magni necessitatibus repellendus voluptate!',
        	'price'=>'1200', 
            'status'=>'Active'
        ]);
        $product->save();
    
      
        $product = new \App\Product([
        	'imagePath'=>'https://upload.wikimedia.org/wikipedia/commons/thumb/a/a0/Wedding_cake_with_pillar_supports%2C_2009.jpg/1200px-Wedding_cake_with_pillar_supports%2C_2009.jpg',
        	'product_name'=>'Cakes',
        	'description'=>'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolores error eum inventore officia quis quos totam! Asperiores deleniti, distinctio illum incidunt nulla officiis quas suscipit vitae? Magni necessitatibus repellendus voluptate!',
        	'price'=>'1300', 
            'status'=>'Active'


        ]);
        $product->save();
    
      
        $product = new \App\Product([
        	'imagePath'=>'https://pic.cakesdecor.com/m/q0zqrwxdiicjrrmurtzv.jpg',
        	'product_name'=>'Cakes',
        	'description'=>'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolores error eum inventore officia quis quos totam! Asperiores deleniti, distinctio illum incidunt nulla officiis quas suscipit vitae? Magni necessitatibus repellendus voluptate!',
        	'price'=>'1000', 
            'status'=>'Active'


        ]);
        $product->save();
    
      
        $product = new \App\Product([
        	'imagePath'=>'https://graybarnbaking.files.wordpress.com/2017/08/ic-cone-cake_11.jpg?w=243&h=431',
        	'product_name'=>'IC Cone Cake',
        	'description'=>'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolores error eum inventore officia quis quos totam! Asperiores deleniti, distinctio illum incidunt nulla officiis quas suscipit vitae? Magni necessitatibus repellendus voluptate!',
        	'price'=>'1000', 
            'status'=>'Active'


        ]);
        $product->save();
    
         
        $product = new \App\Product([
        	'imagePath'=>'https://s3-ap-southeast-1.amazonaws.com/blog-ph/wp-content/uploads/2017/01/26031216/photo-2.jpeg',
        	'product_name'=>'Cakes',
        	'description'=>'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolores error eum inventore officia quis quos totam! Asperiores deleniti, distinctio illum incidunt nulla officiis quas suscipit vitae? Magni necessitatibus repellendus voluptate!',
        	'price'=>'1000', 
            'status'=>'Active'

        ]);
        $product->save();
    }
}
