<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         User:: create([
        	'name'=>'Joy Omesa',
        	'email'=>'joyomesa@gmail.com',
        	'password'=> Hash::make('password'),
        	'role'=>'admin',
        	'status'=>'1',
        	'remember_token' =>Str::random(10)
        ]);
    }
}
