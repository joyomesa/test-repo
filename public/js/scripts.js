 $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

$(document).ready(function(){
	$("#accountdropdown").on("mouseover",function(){
		$(".user-account").show();
	});

	$("#accountdropdown").on("mouseout",function(){
		$(".user-account").hide();
	});

	$(".close").on("click",function(){
		$(".statusmsg").hide();
	});
})


// SUBMIT SIGN UP FORM
$(document).ready(function(){
	
	$("#signupform").parsley();

	$("#signupform").on("submit", function(event){

			event.preventDefault();
		//	
		
		if($("#signupform").parsley().isValid()){
			//alert("form submitted");

			$.ajax({
				type:"post",
				url:"post-signup",
				data:$(this).serialize(),
				dataType:"json",
				success:function(response){
					$(".statusmsg").show();
					$("#successmsg").text(response.msg);
					if(response.status>0){
						$("#signupform").parsley().reset();
						$("#signupform")[0].reset();
					}	else{
						$("#successmsg").removeClass('text-success');
						$("#successmsg").addClass('text-danger');
						$(".statusmsg").removeClass('alert-success');
						$(".statusmsg").addClass('alert-danger');
					}			
				}
			});
		}
	});
});

//SUBMIT LOGIN FORM 

$(document).ready(function(){
	$("#loginform").parsley();

	$("#loginform").on("submit", function(event){
		event.preventDefault();

		if($("#loginform").parsley().isValid()){
			$.ajax({
				type:"post",
				url:"post-login",
				dataType:"json",
				data: $(this).serialize(),
				success:function(response){
					$(".statusmsg").show();
					$("#successmsg").text(response.msg);
					if(response.status>0){
						$("#loginform").parsley().reset();
						$("#loginform")[0].reset();
						window.location.href = "/home";
					}else{
						$("#successmsg").removeClass("text-success");
						$("#successmsg").addClass("text-danger");
						$(".statusmsg").removeClass("alert-success");
						$(".statusmsg").addClass("alert-danger");
					}
				}
			});
		}
	});
});

//LOAD PRODUCTS TO VIEW 

$(document).ready(function(){
	$.ajax({
		url:"getproducts",
		type:"get",
		dataType:"json",
		success:function(data){
		/*	var i=0;
			console.log(data);
			var products = "<li> <div class='thumbnail'><img src='"+data[i].imagePath+"' alt='...' class='img-responsive'>"+
			"<div class='caption'>"+
                    "<h3>"+data[i].product_name+"</h3>"+
                    "<p class='description'>"+data[i].description+"</p>"+
                     "<div class='clearfix'>"
                        "<div class='pull-left price'>KSH "+data[i].price+"</div>"+
                        "<a href='#'' class='btn btn-success pull-right' role='button'>Add to Cart</a>"+
                    "</div>"+
                "</div>"+
            "</div></li>";
			$("#products_list").append(products);*/
		}
	});
});


function addtoCart(productid){
	var currentcartqty =  $("#cart-holder").text();
	let totalcartitems = parseInt(currentcartqty)+1;

	 $("#cart-holder").text(totalcartitems);
	 $("#successmsg").text("Item added to cart!");
	 $(".statusmsg").show();
	/*$.ajax({
		url:"addtocart",
		type:""
	});*/
}