<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable =[
    	'imagePath',
    	'product_name',
    	'description',
    	'price',
    	'status'
    ];
}
