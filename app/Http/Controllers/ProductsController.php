<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


use Illuminate\Auth\Access\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

use Illuminate\Support\Str;
use Illuminate\Support\Arr;
use App\User;
use App\Product;

class ProductsController extends Controller
{
    public function index(){
    	$products  = DB::table('products')
    		->get();

    		return response()->json($products);
    		  }
}
