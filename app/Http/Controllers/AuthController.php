<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Auth\Access\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Session;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Support\Arr;
use App\User;


class AuthController extends Controller{

  
    public function home(){
        return view( "shop.products");
    }

//INVOKE THE SIGNUP
    public function create(Request $request){
    	if(Auth::check()){
    		return view('shop.products');
    	}
    	else{
    		return view('user.signup');
    	}
    }
//INVOKE THE LOGIN VIEW
    public function login(){
    	if(Auth::check()){
    		return view('shop.products');
    	}else{
    		return view('user.login');
    	}
    }

    //POST LOGIN FORM 
public function postlogin(Request $request){
	$user_data = array(
    		'email'=>$request->get('email'),
    		'password'=>$request->get('password')
    	);

    	if(Auth::attempt($user_data)){
           $user_data = Auth::user();
            Session::put("userid",$user_data->name);
            $status = 1; $msg = "Welcome, ".Auth::user()->name;
          
    	}else{
    		  $status = 0; $msg ="Email or password is invalid!";
            
    	}
 return response()->json(['status'=>$status,'msg'=>$msg]);
}


//POST USER SIGNUP FORM
    public function store(Request $request){
    	$accounts  = User::where("email",$request->get('email'))
    	->get();
    	if(sizeof($accounts)>0){
    		$status = 0;
    		$msg = "Email address is already taken!";
    	}else{
    		$data = $request->all();
    		$userdata = [
    			'name'=>$data['fullname'],           		 
            	'email'=>$data['email'],
            	'role'=>'user',
            	'password'=> Hash::make($data['password']),
            	'status'=>'1',
            	'remember_token' =>Str::random(10)
        	];
    		$response = User::create($userdata);
    		if(isset($response)){
    			$msg = "Account created successfully!";
    			$status = 1;
    		}else{
    			$msg = "Something went wrong. Try again!";
    			$status = 0;
    		}
    	}
    	return response()->json(["status"=>$status,"msg"=>$msg]);
    }


    public function logout(){
    	Auth::logout();
    	Session::flush();
    	return redirect('login');
    }



     public function update(Request $request, $id){

     }
     public function edit($id){


     }

     public function destroy($id){

     }
}
